Rid of 2.7 and 3.6
==================

Done
----

- [act] no more option `-python`
- [cmake] no more python2 (hopefully)
- [pyAgrum] no more `generated-files{2|3}` but a new `generated-files`
- [CI] CI
- [pyAgrum] Some import warnings
- [pyAgrum] Metadatas
- [pyAgrum] `"{}".format(x)` to f-string `f"{x}"`
- [pyAgrum] rid of 'import six' and 'from __future__'
- [pyAgrum] linter and cie in pyAgrum.lib

To Do
-----

- packages pip 