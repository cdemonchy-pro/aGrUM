/**
 *
 *   Copyright (c) 2005-2021 by Pierre-Henri WUILLEMIN(_at_LIP6) & Christophe GONZALES(_at_AMU)
 *   info_at_agrum_dot_org
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


/**
 * @file
 * @brief A generic class to combine efficiently several MultiDim tables.
 *
 * @author Christophe GONZALES(_at_AMU) and Pierre-Henri WUILLEMIN(_at_LIP6)
 */

#ifndef DOXYGEN_SHOULD_SKIP_THIS

#  include <agrum/agrum.h>

namespace gum {

  // Constructor
  template < typename GUM_SCALAR, template < typename > class TABLE >
  MultiDimCombination< GUM_SCALAR, TABLE >::MultiDimCombination() {
    GUM_CONSTRUCTOR(MultiDimCombination);
  }

  // Copy constructor
  template < typename GUM_SCALAR, template < typename > class TABLE >
  MultiDimCombination< GUM_SCALAR, TABLE >::MultiDimCombination(
     const MultiDimCombination< GUM_SCALAR, TABLE >& from) {
    GUM_CONS_CPY(MultiDimCombination);
  }

  // Destructor
  template < typename GUM_SCALAR, template < typename > class TABLE >
  MultiDimCombination< GUM_SCALAR, TABLE >::~MultiDimCombination() {
    GUM_DESTRUCTOR(MultiDimCombination);
  }

} /* namespace gum */

#endif /* DOXYGEN_SHOULD_SKIP_THIS */
