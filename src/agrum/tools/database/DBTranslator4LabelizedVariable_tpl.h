/**
 *
 *   Copyright (c) 2005-2021 by Pierre-Henri WUILLEMIN(_at_LIP6) & Christophe GONZALES(_at_AMU)
 *   info_at_agrum_dot_org
 *
 *  This library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


/** @file
 * @brief The databases' cell translators for labelized variables
 *
 * @author Christophe GONZALES(_at_AMU) and Pierre-Henri WUILLEMIN(_at_LIP6)
 */

#include <utility>
#include <vector>

#include <agrum/tools/database/DBTranslator4LabelizedVariable.h>
#include <agrum/tools/database/DBCell.h>

#ifndef DOXYGEN_SHOULD_SKIP_THIS

namespace gum {

  namespace learning {


    /// default constructor
    template < template < typename > class ALLOC >
    template < template < typename > class XALLOC >
    DBTranslator4LabelizedVariable< ALLOC >::DBTranslator4LabelizedVariable(
       const std::vector< std::string, XALLOC< std::string > >&                missing_symbols,
       std::size_t                                                             max_dico_entries,
       const typename DBTranslator4LabelizedVariable< ALLOC >::allocator_type& alloc) :
        DBTranslator< ALLOC >(DBTranslatedValueType::DISCRETE,
                              true,
                              missing_symbols,
                              true,
                              max_dico_entries,
                              alloc),
        _variable_("var", "", 0) {
      GUM_CONSTRUCTOR(DBTranslator4LabelizedVariable);
    }


    /// default constructor without missing symbols
    template < template < typename > class ALLOC >
    DBTranslator4LabelizedVariable< ALLOC >::DBTranslator4LabelizedVariable(
       std::size_t                                                             max_dico_entries,
       const typename DBTranslator4LabelizedVariable< ALLOC >::allocator_type& alloc) :
        DBTranslator< ALLOC >(DBTranslatedValueType::DISCRETE, true, true, max_dico_entries, alloc),
        _variable_("var", "", 0) {
      GUM_CONSTRUCTOR(DBTranslator4LabelizedVariable);
    }


    /// default constructor with a labelized variable as translator
    template < template < typename > class ALLOC >
    template < template < typename > class XALLOC >
    DBTranslator4LabelizedVariable< ALLOC >::DBTranslator4LabelizedVariable(
       const LabelizedVariable&                                                var,
       const std::vector< std::string, XALLOC< std::string > >&                missing_symbols,
       const bool                                                              editable_dictionary,
       std::size_t                                                             max_dico_entries,
       const typename DBTranslator4LabelizedVariable< ALLOC >::allocator_type& alloc) :
        DBTranslator< ALLOC >(DBTranslatedValueType::DISCRETE,
                              true,
                              missing_symbols,
                              editable_dictionary,
                              max_dico_entries,
                              alloc),
        _variable_(var) {
      // check that the variable has not too many entries
      if (var.domainSize() > max_dico_entries) {
        GUM_ERROR(SizeError, "the dictionary induced by the variable is too large")
      }

      // add the content of the variable into the back dictionary
      std::size_t size = 0;
      for (const auto& label: var.labels()) {
        // if the label corresponds to a missing value, then remove it from
        // the set of missing symbols.
        if (this->missing_symbols_.exists(label)) { this->missing_symbols_.erase(label); }

        // insert the label into the back_dictionary
        this->back_dico_.insert(size, label);
        ++size;
      }

      GUM_CONSTRUCTOR(DBTranslator4LabelizedVariable);
    }


    /// default constructor with a labelized variable as translator
    template < template < typename > class ALLOC >
    DBTranslator4LabelizedVariable< ALLOC >::DBTranslator4LabelizedVariable(
       const LabelizedVariable&                                                var,
       const bool                                                              editable_dictionary,
       std::size_t                                                             max_dico_entries,
       const typename DBTranslator4LabelizedVariable< ALLOC >::allocator_type& alloc) :
        DBTranslator< ALLOC >(DBTranslatedValueType::DISCRETE,
                              true,
                              editable_dictionary,
                              max_dico_entries,
                              alloc),
        _variable_(var) {
      // check that the variable has not too many entries
      if (var.domainSize() > max_dico_entries) {
        GUM_ERROR(SizeError, "the dictionary induced by the variable is too large")
      }

      // add the content of the variable into the back dictionary
      std::size_t size = 0;
      for (const auto& label: var.labels()) {
        // insert the label into the back_dictionary
        this->back_dico_.insert(size, label);
        ++size;
      }

      GUM_CONSTRUCTOR(DBTranslator4LabelizedVariable);
    }


    /// copy constructor with a given allocator
    template < template < typename > class ALLOC >
    INLINE DBTranslator4LabelizedVariable< ALLOC >::DBTranslator4LabelizedVariable(
       const DBTranslator4LabelizedVariable< ALLOC >&                          from,
       const typename DBTranslator4LabelizedVariable< ALLOC >::allocator_type& alloc) :
        DBTranslator< ALLOC >(from, alloc),
        _variable_(from._variable_) {
      GUM_CONS_CPY(DBTranslator4LabelizedVariable);
    }


    /// copy constructor
    template < template < typename > class ALLOC >
    INLINE DBTranslator4LabelizedVariable< ALLOC >::DBTranslator4LabelizedVariable(
       const DBTranslator4LabelizedVariable< ALLOC >& from) :
        DBTranslator4LabelizedVariable< ALLOC >(from, from.getAllocator()) {}


    /// move constructor with a given allocator
    template < template < typename > class ALLOC >
    INLINE DBTranslator4LabelizedVariable< ALLOC >::DBTranslator4LabelizedVariable(
       DBTranslator4LabelizedVariable< ALLOC >&&                               from,
       const typename DBTranslator4LabelizedVariable< ALLOC >::allocator_type& alloc) :
        DBTranslator< ALLOC >(std::move(from), alloc),
        _variable_(std::move(from._variable_)) {
      GUM_CONS_MOV(DBTranslator4LabelizedVariable);
    }


    /// move constructor
    template < template < typename > class ALLOC >
    INLINE DBTranslator4LabelizedVariable< ALLOC >::DBTranslator4LabelizedVariable(
       DBTranslator4LabelizedVariable< ALLOC >&& from) :
        DBTranslator4LabelizedVariable< ALLOC >(std::move(from), from.getAllocator()) {}


    /// virtual copy constructor with a given allocator
    template < template < typename > class ALLOC >
    DBTranslator4LabelizedVariable< ALLOC >* DBTranslator4LabelizedVariable< ALLOC >::clone(
       const typename DBTranslator4LabelizedVariable< ALLOC >::allocator_type& alloc) const {
      ALLOC< DBTranslator4LabelizedVariable< ALLOC > > allocator(alloc);
      DBTranslator4LabelizedVariable< ALLOC >*         translator = allocator.allocate(1);
      try {
        allocator.construct(translator, *this, alloc);
      } catch (...) {
        allocator.deallocate(translator, 1);
        throw;
      }
      return translator;
    }


    /// virtual copy constructor
    template < template < typename > class ALLOC >
    INLINE DBTranslator4LabelizedVariable< ALLOC >*
           DBTranslator4LabelizedVariable< ALLOC >::clone() const {
      return clone(this->getAllocator());
    }


    /// destructor
    template < template < typename > class ALLOC >
    INLINE DBTranslator4LabelizedVariable< ALLOC >::~DBTranslator4LabelizedVariable() {
      GUM_DESTRUCTOR(DBTranslator4LabelizedVariable);
    }


    /// copy operator
    template < template < typename > class ALLOC >
    DBTranslator4LabelizedVariable< ALLOC >& DBTranslator4LabelizedVariable< ALLOC >::operator=(
       const DBTranslator4LabelizedVariable< ALLOC >& from) {
      if (this != &from) {
        DBTranslator< ALLOC >::operator=(from);
        _variable_                     = from._variable_;
      }

      return *this;
    }


    /// move operator
    template < template < typename > class ALLOC >
    DBTranslator4LabelizedVariable< ALLOC >& DBTranslator4LabelizedVariable< ALLOC >::operator=(
       DBTranslator4LabelizedVariable< ALLOC >&& from) {
      if (this != &from) {
        DBTranslator< ALLOC >::operator=(std::move(from));
        _variable_                     = std::move(from._variable_);
      }

      return *this;
    }


    /// returns the translation of a string, as found in the current dictionary
    template < template < typename > class ALLOC >
    DBTranslatedValue DBTranslator4LabelizedVariable< ALLOC >::translate(const std::string& str) {
      // try to get the index of str within the labelized variable. If this
      // cannot be found, try to find if this corresponds to a missing value.
      // Finally, if this is still not a missing value and, if enabled, try
      // to add str as a new label
      try {
        return DBTranslatedValue{std::size_t(_variable_[str])};
      } catch (gum::Exception&) {
        // check that this is not a missing value
        if (this->isMissingSymbol(str)) {
          return DBTranslatedValue{std::numeric_limits< std::size_t >::max()};
        }

        // try to add str as a new value if possible
        if (this->hasEditableDictionary()) {
          const std::size_t size = _variable_.domainSize();
          if (size >= this->max_dico_entries_)
            GUM_ERROR(SizeError,
                      "String \"" << str << "\" cannot be translated "
                                  << "because the dictionary is already full");
          _variable_.addLabel(str);
          this->back_dico_.insert(size, str);
          return DBTranslatedValue{size};
        } else
          GUM_ERROR(UnknownLabelInDatabase,
                    "The translation of \"" << str << "\" could not be found")
      }
    }


    /// returns the original value for a given translation
    template < template < typename > class ALLOC >
    INLINE std::string DBTranslator4LabelizedVariable< ALLOC >::translateBack(
       const DBTranslatedValue translated_val) const {
      try {
        return this->back_dico_.second(translated_val.discr_val);
      } catch (Exception&) {
        // check if this is a missing value
        if ((translated_val.discr_val == std::numeric_limits< std::size_t >::max())
            && !this->missing_symbols_.empty())
          return *(this->missing_symbols_.begin());
        else
          GUM_ERROR(UnknownLabelInDatabase,
                    "The back translation of \"" << translated_val.discr_val
                                                 << "\" could not be found");
      }
    }


    /// indicates whether the translations should be reordered
    template < template < typename > class ALLOC >
    bool DBTranslator4LabelizedVariable< ALLOC >::needsReordering() const {
      // if the variable contains only numbers, they should be increasing
      const auto& labels      = _variable_.labels();
      float       last_number = std::numeric_limits< float >::lowest();
      float       number;
      bool        only_numbers = true;
      for (const auto& label: labels) {
        if (!DBCell::isReal(label)) {
          only_numbers = false;
          break;
        }
        number = std::stof(label);
        if (number < last_number) return true;
        last_number = number;
      }

      if (!only_numbers) {
        // here we shall examine whether the strings are sorted by
        // lexicographical order
        const std::size_t size = labels.size();
        for (std::size_t i = 1; i < size; ++i) {
          if (labels[i] < labels[i - 1]) return true;
        }
      }

      return false;
    }


    /// returns a mapping to reorder the current dictionary and updates it
    template < template < typename > class ALLOC >
    HashTable< std::size_t, std::size_t, ALLOC< std::pair< std::size_t, std::size_t > > >
       DBTranslator4LabelizedVariable< ALLOC >::reorder() {
      // check whether the variable contains only numeric values. In this
      // case, we have to sort the values by increasing number
      const auto&       labels = _variable_.labels();
      const std::size_t size   = labels.size();

      bool only_numbers = true;
      for (const auto& label: labels) {
        if (!DBCell::isReal(label)) {
          only_numbers = false;
          break;
        }
      }

      // assign to each label its current index
      std::vector< std::pair< std::size_t, std::string >,
                   ALLOC< std::pair< std::size_t, std::string > > >
         xlabels;
      xlabels.reserve(size);
      for (std::size_t i = std::size_t(0); i < size; ++i)
        xlabels.push_back(std::make_pair(i, labels[i]));

      // reorder by increasing order
      if (only_numbers)
        std::sort(xlabels.begin(),
                  xlabels.end(),
                  [](const std::pair< std::size_t, std::string >& a,
                     const std::pair< std::size_t, std::string >& b) -> bool {
                    return std::stof(a.second) < std::stof(b.second);
                  });
      else
        std::sort(xlabels.begin(),
                  xlabels.end(),
                  [](const std::pair< std::size_t, std::string >& a,
                     const std::pair< std::size_t, std::string >& b) -> bool {
                    return a.second < b.second;
                  });

      // check whether there were any modification
      bool modifications = false;
      for (std::size_t i = std::size_t(0); i < size; ++i) {
        if (xlabels[i].first != i) {
          modifications = true;
          break;
        }
      }

      // if there were no modification, return an empty update hashtable
      if (!modifications) {
        return HashTable< std::size_t,
                          std::size_t,
                          ALLOC< std::pair< std::size_t, std::size_t > > >();
      }

      // recreate the variable so that the labels correspond to the
      // new ordering
      _variable_.eraseLabels();
      for (auto& label: xlabels)
        _variable_.addLabel(label.second);

      // create the hashTable corresponding to the mapping from the old
      // indices to the new one
      this->back_dico_.clear();
      HashTable< std::size_t, std::size_t, ALLOC< std::pair< std::size_t, std::size_t > > > mapping(
         (Size)size);
      for (std::size_t i = std::size_t(0); i < size; ++i) {
        mapping.insert(xlabels[i].first, i);
        this->back_dico_.insert(i, xlabels[i].second);
      }

      return mapping;
    }


    /// returns the domain size of a variable corresponding to the translations
    template < template < typename > class ALLOC >
    INLINE std::size_t DBTranslator4LabelizedVariable< ALLOC >::domainSize() const {
      return _variable_.domainSize();
    }


    /// returns the variable stored into the translator
    template < template < typename > class ALLOC >
    INLINE const LabelizedVariable* DBTranslator4LabelizedVariable< ALLOC >::variable() const {
      return &_variable_;
    }


    /// returns the translation of a missing value
    template < template < typename > class ALLOC >
    INLINE DBTranslatedValue DBTranslator4LabelizedVariable< ALLOC >::missingValue() const {
      return DBTranslatedValue{std::numeric_limits< std::size_t >::max()};
    }


  } /* namespace learning */

} /* namespace gum */


#endif /* DOXYGEN_SHOULD_SKIP_THIS */
