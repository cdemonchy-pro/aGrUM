Module dynamic Bayesian network
===============================

.. image:: _static/dBN.png

.. automodule:: pyAgrum.lib.dynamicBN
    :members:
    :undoc-members:
    :show-inheritance:
    :noindex:
