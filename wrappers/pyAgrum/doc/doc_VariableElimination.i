%feature("docstring") gum::VariableElimination
"
Class used for Variable Elimination inference algorithm.

VariableElimination(bn) -> VariableElimination
    Parameters:
        * **bn** (*pyAgrum.BayesNet*) -- a Bayesian network
"